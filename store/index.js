import { createStore } from "redux";
import cartItems from "../../ReactNativeFacebookAuth/reducers/cartItems";

const store = createStore(cartItems);
export default store;

import React, { useState } from "react";
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  state,
  ActivityIndicator
} from "react-native";

const phoneDetails = props => {
  const [indicator, setIndicator] = useState(false);

  return (
    <View style={styles.container}>
      <Text style={styles.EnterText}>Enter your Mobile Number</Text>
      <TextInput
        // onPress={() => this.props.navigation.navigate("Phone")}
        placeholder="+91 8073141091"
        underlineColorAndroid="transparent"
        style={styles.TextInputStyleClass1}
      />
      <TouchableOpacity
        onPress={() => props.navigation.navigate("Cart")}
        style={styles.TextInputStyleClass2}
      >
        <Text style={styles.buttonText}>Verify</Text>
      </TouchableOpacity>
      <ActivityIndicator size="large" color="orange" animating={indicator} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb3b3"
  },

  TextInputStyleClass1: {
    textAlign: "center",
    height: 50,
    borderWidth: 2,
    borderColor: "#FF5722",
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    top: 150,
    margin: 50
  },
  EnterText: {
    top: 190,
    marginLeft: 70,
    fontSize: 17
  },
  TextInputStyleClass2: {
    textAlign: "center",
    height: 30,
    borderWidth: 2,
    borderColor: "#FF5722",
    borderRadius: 15,
    backgroundColor: "#FFFFFF",
    margin: 130,
    width: 200,
    backgroundColor: "#FF5722",
    right: 45,
    top: 1
  },
  buttonText: {
    textAlign: "center",
    fontSize: 17
  }
});
export default phoneDetails;

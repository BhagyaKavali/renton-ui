import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { withNavigation } from "react-navigation";
class CounterApp extends Component {
  state = {
    counter: 0
  };
  increaseCounter = () => {
    this.setState({ counter: this.state.counter + 1 });
  };
  decrease = () => {
    this.setState({ counter: this.state.counter - 1 });
  };

  render() {
    return (
      <View style={StyleSheet.container}>
        <View
          style={{
            flexDirection: "row",
            width: 200,
            justifyContent: "space-around"
          }}
        >
          <TouchableOpacity onPress={() => this.increaseCounter()}>
            <Text style={{ fontSize: 20 }}>Increase</Text>
          </TouchableOpacity>
          <Text style={{ fontsize: 20 }}>{this.state.counter}</Text>
          <TouchableOpacity onPress={() => this.decreaseCounter()}>
            <Text style={{ fontSize: 20 }}>Decrease</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

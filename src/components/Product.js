// src/components/Product.js
import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import { Card, Button } from "react-native-elements";
import { withNavigation } from "react-navigation";
import { FontAwesome5 } from "@expo/vector-icons";
import StarRating from "../views/StarRating";
import { MaterialIcons } from "@expo/vector-icons";
import Like from "../views/like";

const products = [
  {
    Pid: 1,
    name: "Lawn Mower",
    price: "$ 5/hr",
    img: require("../image/shop.jpg"),
    rent: "63 times",
    loc: "Est. 6.3 KMs"
  },
  {
    Pid: 2,
    name: "Driller Chordless",
    price: "$ 5/hr",
    img: require("../image/driller.jpg"),
    rent: "123 times",
    loc: "Est. 6.1 KMs"
  },
  {
    Pid: 3,
    name: "Lawn Tractor",
    price: "$ 15/hr",
    img: require("../image/tractor.jpg"),
    rent: "12 times",
    loc: "Est. 9.1 KMs"
  },
  {
    Pid: 4,
    name: "Rake",
    price: "$ 1/hr",
    img: require("../image/rake.png"),
    rent: "2 times",
    loc: "Est. 1.1 KMs"
  }
];

class Product extends React.Component {
  render() {
    return products.map((product, index) => {
      console.log(product.name);

      return (
        <Card>
          <Text style={{ marginBottom: 10, marginTop: 20 }} h4>
            {product.name}
          </Text>
          <Text style={styles.price} h4>
            {product.price}
            <Text style={styles.container}>
              <StarRating />
            </Text>
          </Text>
          <Text h6 style={styles.description}>
            added 2h ago
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("ProductDetails", {
                name: product.name,

                price: product.price,

                img: product.img
              })
            }
          >
            <Card
              image={product.img}
              containerStyle={{ elevation: 0, borderColor: "transparent" }}
            />
          </TouchableOpacity>

          <Card style={{ backgroundColor: "blue" }}>
            <Text style={{ color: "purple" }}>
              Rented : {product.rent}
              <FontAwesome5
                name="shopping-bag"
                size={20}
                color="purple"
                style={{ paddingLeft: 100 }}
              />
            </Text>
            <br></br>
            <Text style={{ color: "green" }}>
              <MaterialIcons name="location-on" size={22} color="green" />
              {product.loc}
              <Text style={{ paddingLeft: 110 }}>
                <Like />
              </Text>
            </Text>
          </Card>
        </Card>
      );
    });
  }
}

const styles = StyleSheet.create({
  name: {
    color: "#5a647d",
    fontWeight: "bold",
    fontSize: 30
  },
  price: {
    fontWeight: "bold",
    marginBottom: 10
  },
  description: {
    fontSize: 10,
    color: "#c1c4cd"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItem: "center",
    paddingLeft: 150
  }
});

export default withNavigation(Product);

import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import HomeScreen from "./views/Home";
import DetailScreen from "./views/Details";
import shappingcart from "./components/shoppingcart";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { EvilIcons } from "@expo/vector-icons";
import { createAppContainer } from "react-navigation";
import phoneDetails from "./components/phoneDetails";
import DesignScreen from "./views/Design";
import ProductDetailsScreen from "./views/ProductDetails";

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: "Discover",
        headerRight: (
          <MaterialIcons name="search" size={24} color="purple" padding />
        ),

        headerLeft: <Entypo name="menu" size={24} color="purple" />,
        headerTop: <Text>0</Text>,
        headerTintColor: "purple",
        headerTitleStyle: {
          fontWeight: "bold",
          textAlign: "center"
        }
      }
    },
    shoppingcart: {
      screen: shappingcart,
      navigationOptions: {
        header: null
      }
    },
    phoneDetails: {
      //  title:"send",
      screen: phoneDetails,
      navigationOptions: {
        header: null
      }
    },
    ProductDetails: {
      screen: ProductDetailsScreen,
      navigationOptions: {
        header: null
      }
    },
    Details: {
      screen: DetailScreen,
      navigationOptions: {
        header: null
      }
    },
    Design: {
      screen: DesignScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Home"
  }
);

export default AppNavigator;
